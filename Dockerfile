FROM maven:3.5.3-jdk-11
COPY pom.xml .
RUN mvn dependency:resolve 2>&1 | grep -v "Downloading from" | grep -v "Downloaded from"
RUN mvn dependency:resolve-plugins 2>&1 | grep -v "Downloading from" | grep -v "Downloaded from"
RUN rm pom.xml
